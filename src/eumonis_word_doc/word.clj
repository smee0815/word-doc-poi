(ns ^{:doc "Each word 97-2003 file consists of a Range. A range contains paragraphs and sections. A paragraph may contain tables and character runs.
A table consists of rows, each row is a sequence of cells."
      :author "Steffen Dienst"} eumonis-word-doc.word
  (:use [clojure.java.io :only (input-stream file)])
  (:require [clojure.string :as string])
  (:import org.apache.poi.poifs.filesystem.POIFSFileSystem
           org.apache.poi.hwpf.HWPFDocument
           org.apache.poi.hwpf.usermodel.HeaderStories
           org.apache.poi.hwpf.usermodel.Range)
  (:gen-class)) 


(defn load-doc 
  "Read a Word 97-2003 document. Location may be a URL, filename, inputstream of reader."
  [location]
  (let [poifs (POIFSFileSystem. (input-stream location))
        document (HWPFDocument. poifs)]
    document))

(defn read-header [^HWPFDocument document]
  (let [hs (HeaderStories. document)] 
    [(.getEvenHeader hs) (.getOddHeader hs)]))

(defn read-footer [^HWPFDocument document]
  (let [hs (HeaderStories. document)] 
    [(.getFirstFooter hs) (.getEvenFooter hs) (.getOddFooter hs)]))

(defn get-range [^HWPFDocument document]
  (.getRange document))

(defn stylesheet [^HWPFDocument document]
  (.getStyleSheet document))

(defn get-style [stylesheet paragraph]
  (.getName (.getStyleDescription stylesheet (.getStyleIndex paragraph))))

(defmacro def-word [name num-method getter-method]
  `(defn ~name [param#]
     (map #(~getter-method param# %) (range (~num-method param#)))))

(def-word paragraphs .numParagraphs .getParagraph)
(def-word sections .numSections .getSection)
(def-word rows .numRows .getRow)
(def-word cells .numCells .getCell)
(def-word character-runs .numCharacterRuns .getCharacterRun)

(defn table [r paragraph]
  (try (.getTable r paragraph)
    (catch Exception _ nil)))

(defn text [^Range r]
  (when r
    (let [t (-> r .text Range/stripFields (string/replace "\r" "\r\n"))] 
      (string/trim t))))

(defn get-level [list-entry]
  (.getIlvl list-entry))