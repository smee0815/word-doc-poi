(ns eumonis-word-doc.eumonis-requirements
  (:require [clojure.string :as string])
  (:use eumonis-word-doc.word
        [clojure.java.io :only (file)]
        [org.clojars.smee.seq :only (partition-when)])
  (:import [org.apache.poi.hwpf.usermodel Paragraph ListEntry Table])
  (:gen-class))

;;;;;;;;;;;;; eumonis specific functions ;;;;;;;;;;;;;;;;;;

(defn req-priority 
  "Extract priority field: Take bold text only."
  [req-table]
  (let [cell (-> req-table rows second cells second)
        p (first (paragraphs cell))
        cr (character-runs p)]
    (text (first (filter #(.isBold %) cr)))))

;;(defn req-priority [req-table] (-> req-table rows second cells second text))

(defn prefix-of-ul [list-entry]
  (apply str (-> list-entry get-level inc (repeat \*))))

(defn req-texts 
  "Clean up requirement text: Replace nested unordered lists with seqs of *. Up to nesting level 4.
Split requirements text whenever a new paragraph starts that is not an instance of ListEntry."
  [req-table]
  (let [c (-> req-table rows (nth 2) cells second)
        p (paragraphs c)
        ;; split requirements using heuristic: each paragraph is a new requirements if it's successor is no ListEntry
        reqs (->> (concat p (take-last 1 p)) 
               (partition 2 1 ) 
               (partition-when (fn [[_ s]] (not (instance? ListEntry s)))) 
               (map (partial map first)))]
    (for [req reqs]
      (for [p req] 
        (if (= (class p) ListEntry)
          (str (prefix-of-ul p) \space (text p))
          (text p))))))

(defn req-text [req-texts]
  (string/join "\r\n" req-texts)) 

(defn eum-requirement? [table]
  (and (instance? Table table) 
       (= "SR-ID" (-> table rows first cells first text))))

(defn- maybe-extract-requirements [range [h t]]
  (when-let [t (table range t)] 
    (when (eum-requirement? t)
      (let [r-texts (req-texts t)
            r-prio (req-priority t)
            r (apply hash-map (mapcat #(map text (cells %)) (rows t)))
            r (assoc r "Name" (text h) "Priorität" r-prio)]
        (for [parts r-texts] 
          (assoc r "Beschreibung" (req-text parts)))))))

(defn extract-eumonis-requirements [d]
  (let [r (get-range d)
        p (paragraphs r)
        reqs (->> p 
               (partition 2 1) 
               (keep #(maybe-extract-requirements r %))
               flatten)]
    reqs))



(defn -main [& args]
  (doseq [n args :let [d (load-doc n)
                       out (file (str n ".csv"))]]
    (do 
      (println "Extracting requirements from" n "into" out)
      (spit out
        (with-out-str
          (let [keys ["SR-ID" "Name" "Ansprechpartner" "Priorität" "Referenz" "Abnahmekriterium" "Letzte Änderung durch" "Beschreibung" "Kommentar"]]
            (println (string/join ";" (map pr-str keys)))
            (doseq [req (extract-eumonis-requirements d)]
              (println (string/join ";" (map (comp pr-str req) keys))))))))))

(comment
  ;; example generating a TopCASED requirement model from an EUMONIS req. document
  (doseq [req (extract-eumonis-requirements (load-doc "e:/temp/test.doc"))]
    (println "<children xmi:type=\"ttm:Requirement\" ident=\"" (get req "Name") "\">")
    (doseq [k ["Priorität" "Ansprechpartner" "SR-ID" "Beschreibung" "Referenz" "Abnahmekriterium" "Kommentar" "Letzte Änderung durch"]]
      (printf "   <attributes name=\"%s\" value=\"%s\"/>\n" k (get req k)))
    (println "</children>"))
  
  
  )